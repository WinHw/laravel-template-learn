<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        $user = new User([
            'name' => 'Erwin Zhang',
            'email' => 'erwin.zhang@springkraf.com',
            'password' => Hash::make('secret'),
        ]);
        $user->saveOrFail();
    }
}
