<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    public function run(): void
    {
        $employee = new Employee([
            'name' => 'Super Admin',
            'email' => 'admin@springkraf.com',
            'password' => Hash::make('secret'),
        ]);
        $employee->saveOrFail();
    }
}
