<?php

return [
    'permissions' => [
        'employee.roles.page',
        'employee.roles.list',
        'employee.roles.view',
        'employee.roles.create',
        'employee.roles.update',
        'employee.roles.delete',

        'employee.employees.page',
        'employee.employees.list',
        'employee.employees.view',
        'employee.employees.create',
        'employee.employees.update',
    ],

    'permission_groups' => [
        'employee.account' => [
            'employee.roles:manage' => [
                'employee.roles.page',
                'employee.roles.list',
                'employee.roles.view',
                'employee.roles.create',
                'employee.roles.update',
                'employee.roles.delete',
            ],
            'employee.employees:manage' => [
                'employee.employees.page',
                'employee.employees.list',
                'employee.employees.view',
                'employee.employees.create',
                'employee.employees.update',

                'employee.roles.list',
                'employee.roles.view',
            ],
        ],
    ],
];
