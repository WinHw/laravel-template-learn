ARG PROJECT

FROM asia-southeast1-docker.pkg.dev/$PROJECT/core/swoole:5.0-php-8.1-v3

COPY .docker/production/swoole/99-overrides.ini /usr/local/etc/php/conf.d/99-overrides.ini
COPY . .

# https://github.com/deployphp/deployer/blob/master/recipe/deploy/vendors.php
RUN composer install --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader

RUN php artisan view:cache && \
    php artisan route:cache && \
    php artisan event:cache

CMD ["php", "artisan", "octane:start", "--host=0.0.0.0", "--port=8080", "--workers=4", "--max-requests=1000"]
