<?php

use App\Http\Controllers\AccountController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AccountController::class, 'retrieve'])->name('retrieve-multi');
Route::get('/deleted', [AccountController::class, 'retrieveSoftDeleted'])->name('retrieve-soft-deleted');
Route::post('signup', [AccountController::class, 'signup'])->name('signup');
Route::post('login', [AccountController::class, 'login'])->name('login');
Route::patch('/{id}', [AccountController::class, 'updateByID'])->name('update-by-id');
Route::delete('/{id}', [AccountController::class, 'softDelete'])->name('soft-delete');
Route::delete('self', [AccountController::class, 'selfSoftDelete'])->name('self-soft-delete');
Route::delete('self-force', [AccountController::class, 'selfForceDelete'])->name('self-force-delete');
