<?php

use App\Http\Controllers\GlobalController;
use App\Http\Controllers\User\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('health', [GlobalController::class, 'health'])->name('health');

Route::post('auth/login', [AuthController::class, 'login'])->name('auth.login');
Route::post('auth/refresh', [AuthController::class, 'refresh'])->name('auth.refresh');

Route::middleware('auth:user_api')->group(function () {
    Route::post('auth/revoke', [AuthController::class, 'revoke'])->name('auth.revoke');

    Route::prefix('me')->group(static function () {
        Route::get('', [AuthController::class, 'getMe'])->name('me');
        Route::post('auth/change-password', [AuthController::class, 'changePassword'])->name('change-password');
    });
});
