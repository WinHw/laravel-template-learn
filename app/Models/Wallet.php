<?php

namespace App\Models;

use App\Models\Abstract\BaseUuidModel;
use App\Models\Common\WalletType;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends BaseUuidModel
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'amount',
        'type',
    ];
    protected $casts = [
        'type' => WalletType::class
    ];

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }
}
