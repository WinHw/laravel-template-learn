<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role as BaseRole;
use Watson\Validating\ValidatingTrait;

class Role extends BaseRole
{
    use ValidatingTrait;

    public $incrementing = false;

    protected $keyType = 'string';

    protected $dateFormat = 'Y-m-d\TH:i:s.uP';

    protected $hidden = ['guard_name'];

    protected $fillable = ['name'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->assignPrimaryKey();
    }

    protected function assignPrimaryKey(): void
    {
        $key = $this->getKeyName();
        if ($this->$key === null) {
            $this->$key = Str::ulid()->toRfc4122();
        }
    }

    public function getRules(): array
    {
        return [
            'name' => 'required',
            'guard_name' => 'required',
        ];
    }

    public function permissionGroups(): BelongsToMany
    {
        return $this->belongsToMany(
            PermissionGroup::class,
            'role_has_permission_groups',
            'role_id',
            'permission_group_id'
        );
    }
}
