<?php

namespace App\Models;

use App\Models\Abstract\BaseUuidModel;
use OwenIt\Auditing\Audit as Auditing;
use OwenIt\Auditing\Contracts\Audit as AuditContract;

class Audit extends BaseUuidModel implements AuditContract
{
    use Auditing;

    protected $guarded = [];

    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json',
        'user_id' => 'string',
    ];
}
