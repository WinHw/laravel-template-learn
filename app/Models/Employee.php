<?php

namespace App\Models;

use App\Models\Abstract\BaseUuidUser;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Watson\Validating\ValidatingTrait;

class Employee extends BaseUuidUser
{
    use Notifiable;
    use ValidatingTrait;
    use HasRoles;
    use HasApiTokens;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setEmailAttribute(string $value): void
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function getRules(): array
    {
        return [
            'email' => 'required|email|unique:employees,email,'.$this->id,
            'password' => 'required|string|min:6',
            'name' => 'required|string',
        ];
    }
}
