<?php

namespace App\Models\Common;

use App\Models\Abstract\BaseEnum;

class WalletType extends BaseEnum
{
    const CASH = 'cash';
    const POINT = 'point';

    public static function getConstants(): array
    {
        return (new \ReflectionClass(__CLASS__))->getConstants();
    }

    public static function getConstantValues(): array
    {
        return array_values(self::getConstants());
    }
}
