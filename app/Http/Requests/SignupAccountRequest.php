<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupAccountRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => 'required|regex:/^\S*$/u|unique:accounts,username',
            'email' => 'required|email|unique:accounts,email',
            'password' => 'required|string|confirmed|min:8',
        ];
    }

    public function messages(): array
    {
//        return parent::messages();
        return [
            'username.regex' => 'The username filled must contain no space.',
        ];
    }
}
