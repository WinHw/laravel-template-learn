<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $id = $this->route('id');
        return [
            'username' => 'regex:/^\S*$/u|unique:accounts,username,'.$id,
            'email' => 'email|unique:accounts,email,'.$id,
        ];
    }

    public function messages(): array
    {
        return [
            'username.regex' => 'The username filled must contain no space.',
        ];
    }
}
