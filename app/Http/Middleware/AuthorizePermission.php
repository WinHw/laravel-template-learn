<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class AuthorizePermission
{
    public const AbilityMap = [
        'index' => 'list',
        'show' => 'view',
        'store' => 'create',
        'update' => 'update',
        'destroy' => 'delete',
    ];

    public function handle(Request $request, Closure $next, ?string $permission = null): Response
    {
        $route = $request->route();
        [$ns, $model, $name] = explode('.', $route->getName());
        if ($permission === null) {
            $ability = self::AbilityMap[$name] ?? $name;
            if ($ability === 'update-status') {
                $action = $route->parameters()['action'];
                Gate::authorize("$ns.$model.$action");
            } else {
                Gate::authorize("$ns.$model.$ability");
            }
        } else {
            Gate::authorize("$ns.$permission");
        }

        return $next($request);
    }
}
