<?php

namespace App\Http\Controllers;

use App\Http\Helpers\RequestHelper;
use App\Http\Requests\LoginAccountRequest;
use App\Http\Requests\SignupAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Resources\AccountResource;
use App\Http\Resources\AccountWalletResource;
use App\Models\Account;
use App\Models\Common\WalletType;
use App\Models\Wallet;
use App\QueryBuilder\Employee\AccountQueryBuilder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Knuckles\Scribe\Attributes\Endpoint;
use Knuckles\Scribe\Attributes\Group;
use Knuckles\Scribe\Attributes\UrlParam;
use Throwable;

#[Group('[Employee] Account')]
class AccountController extends Controller
{
    public function __construct(private AccountQueryBuilder $accountQueryBuilder)
    {
    }

    /**
     * Display a listing of the resource.
     */
    #[Endpoint('Account (index)')]
    public function index(Request $request): ResourceCollection
    {
        $accounts = $this->accountQueryBuilder->getQueryBuilder();
        return AccountResource::collection($accounts->paginate(RequestHelper::limit($request)))
            ->additional($this->accountQueryBuilder->getResource($request));
    }

    /**
     * Store a newly created resource in storage.
     * @throws Throwable
     */
    #[Endpoint('Account (store)/(signup)')]
    public function store(SignupAccountRequest $request): AccountResource
    {
        $validated = $request->validated();

        DB::beginTransaction();

        try {
            $account = new Account($validated);
            $account->password = Hash::make($validated['password']);
            $account->saveOrFail();

            $wallets = [];
            foreach(WalletType::getConstantValues() as $type) $wallets[] = new Wallet(['type' => $type]);
            $account->wallets()->saveMany($wallets);

            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
        return (new AccountResource($account))
            ->additional([
                'message' => __('success.store_account_success')
            ]);
    }

    /**
     * Display the specified resource.
     */
    #[Endpoint('Account (show)')]
    #[UrlParam('account')]
    public function show(Account $account): AccountResource
    {
        return new AccountResource($account);
    }

    #[Endpoint('Account (show with wallets)')]
    #[UrlParam('account')]
    public function showWithWallets(Account $account): AccountWalletResource
    {
        return new AccountWalletResource($account);
    }

    #[Endpoint('Account (login)')]
    public function login(LoginAccountRequest $request): AccountResource
    {
        $validated = $request->validated();
        $account = Account::where('email', $validated['credential'])
            ->orWhere('username', $validated['credential'])
            ->first();
        return (new AccountResource($account))
            ->additional([
                'message' => __(
                    (!$account && Hash::check($validated['password'], $account->password)) ?
                        'success.login_account_success' : 'error.login_account_failed'
                )
            ]);
    }

    /**
     * Update the specified resource in storage.
     */
    #[Endpoint('Account (update)')]
    #[UrlParam('account')]
    public function update(UpdateAccountRequest $request, Account $account): AccountResource
    {
        $validated = $request->validated();
        $account->fill($validated);
        try {
            $account->saveOrFail();
            return (new AccountResource($account))
                ->additional([
                    'message' => __('success.update_account_success')
                ]);
        } catch (Throwable $throwable) {
            return (new AccountResource($account))
                ->additional([
                    'message' => __('error.update_account_error'),
                    'error' => $throwable
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    #[Endpoint('Account (destroy)')]
    #[UrlParam('account')]
    public function destroy(Account $account): JsonResponse
    {
        $account->delete();
        return response()->json([
            'message' => __('success.destroy_account_success')
        ]);
    }
}
