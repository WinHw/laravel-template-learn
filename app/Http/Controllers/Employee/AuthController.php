<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Traits\ThrottlesAttempts;
use App\Http\Helpers\RequestHelper;
use App\Http\Resources\EmployeeResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Validation\ValidationException;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\Group;
use Knuckles\Scribe\Attributes\Unauthenticated;
use Laravel\Passport\Exceptions\OAuthServerException;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\TokenRepository;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[Group('EmployeeAuth')]
class AuthController extends AccessTokenController
{
    use ThrottlesAttempts;

    protected function throttleKeyPrefix(): string
    {
        return 'employee_login';
    }

    public function getMe(Request $request): EmployeeResource
    {
        return new EmployeeResource($request->user());
    }

    #[Unauthenticated]
    #[BodyParam('username', 'string')]
    #[BodyParam('password', 'string')]
    public function login(Request $request_http): array
    {
        $this->validateAttempts($request_http);

        $request = RequestHelper::createServerRequest($request_http);
        $body = $request->getParsedBody();
        $body['client_id'] = config('passport.clients.employees.id');
        $body['client_secret'] = config('passport.clients.employees.secret');
        $body['grant_type'] = 'password';
        $body['scope'] = '';

        try {
            $result = json_decode($this->issueToken($request->withParsedBody($body))->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $this->clearAttempts($request_http);
        } catch (OAuthServerException|Exception $exception) {
            $this->incrementAttempts($request_http);
            throw new BadRequestHttpException(__('error.incorrect_credentials'));
        }

        return [
            'data' => $result,
            'message' => __('success.login_success'),
        ];
    }

    #[BodyParam('old_password', 'string')]
    #[BodyParam('password', 'string')]
    #[BodyParam('password_confirmation', 'string')]
    public function changePassword(Request $request): EmployeeResource
    {
        $input = $request->validate([
            'old_password' => 'required|string|min:6',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = $request->user();

        if (! Hash::check($input['old_password'], $user->password)) {
            throw ValidationException::withMessages([
                'old_password' => [__('error.incorrect_password')],
            ]);
        }

        $user->password = Hash::make($input['password']);
        $user->save();

        return (new EmployeeResource($request->user()))
            ->additional([
                'message' => __('success.change_password_success'),
            ]);
    }

    #[BodyParam('refresh_token', 'string')]
    public function refresh(ServerRequestInterface $request): array
    {
        $body = $request->getParsedBody();
        $body['client_id'] = config('passport.clients.employees.id');
        $body['client_secret'] = config('passport.clients.employees.secret');
        $body['grant_type'] = 'refresh_token';
        $body['scope'] = '';

        $key = 'refresh_token:'.$body['refresh_token'];

        $result = null;

        if (Redis::get($key) !== null) {
            $result = json_decode(Redis::get('refresh_token:'.$body['refresh_token']), true, 512, JSON_THROW_ON_ERROR);
        } else {
            Cache::lock($key, 2)->block(3, function () use ($key, $body, $request, &$result) {
                if (Redis::get($key) !== null) {
                    $result = json_decode(Redis::get('refresh_token:'.$body['refresh_token']), true, 512, JSON_THROW_ON_ERROR);
                } else {
                    try {
                        $result = json_decode($this->issueToken($request->withParsedBody($body))->getContent(), true, 512, JSON_THROW_ON_ERROR);
                        Redis::set($key, json_encode($result, JSON_THROW_ON_ERROR, 512), 'EX', 45);
                    } catch (OAuthServerException|Exception $exception) {
                        $result = null;
                    }
                }
            });
        }

        if ($result === null) {
            throw new BadRequestHttpException(__('error.incorrect_refresh_token'));
        }

        return [
            'data' => $result,
            'message' => __('success.refresh_token_success'),
        ];
    }

    public function revoke(): array
    {
        try {
            $tokenRepository = new TokenRepository();
            $refreshTokenRepository = new RefreshTokenRepository();
            $tokenId = auth()->user()->token()->id;
            $tokenRepository->revokeAccessToken($tokenId);
            $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);
        } catch (Exception $e) {
            throw new BadRequestHttpException(__('error.logout_failed'));
        }

        return [
            'message' => __('success.logout_success'),
        ];
    }

    public function getPermission(): array
    {
        return [
            'data' => auth()->user()->getPermissionsViaRoles()->map(fn ($x) => $x['id']),
        ];
    }
}
