<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Helpers\RequestHelper;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Throwable;

class RoleController extends Controller
{
    public function index(Request $request): ResourceCollection
    {
        return RoleResource::collection(Role::paginate(RequestHelper::limit($request)));
    }

    public function show(Request $request, Role $role): RoleResource
    {
        $role->load('permissions', 'permissionGroups');

        return new RoleResource($role);
    }

    public function store(Request $request): RoleResource
    {
        $input = $request->validate([
            'permissions' => 'required|array',
        ]);

        DB::beginTransaction();
        try {
            $role = new Role($request->all());
            $role->guard_name = 'employee_api';
            $role->saveOrFail();

            $permission_ids = DB::table('permission_group_has_permissions')
                ->whereIn('permission_group_id', $input['permissions'])
                ->distinct()->pluck('permission_id');

            $role->syncPermissions($permission_ids);
            $role->permissionGroups()->sync($input['permissions']);
            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }

        $role->load('permissionGroups');

        return new RoleResource($role);
    }

    public function update(Request $request, Role $role): RoleResource
    {
        $input = $request->validate([
            'permissions' => 'required|array',
        ]);

        DB::beginTransaction();
        try {
            $role->fill($request->all());
            $role->saveOrFail();

            $permission_ids = DB::table('permission_group_has_permissions')
                ->whereIn('permission_group_id', $input['permissions'])
                ->distinct()->pluck('permission_id');
            $role->syncPermissions($permission_ids);
            $role->permissionGroups()->sync($input['permissions']);
            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }

        $role->load('permissionGroups');

        return new RoleResource($role);
    }

    public function destroy(Request $request, Role $role): Response
    {
        try {
            $role->delete();
        } catch (Exception $e) {
            throw new BadRequestHttpException(__('error.update_delete_failed'));
        }

        return response()->noContent();
    }
}
