<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Knuckles\Scribe\Attributes\Unauthenticated;

class GlobalController
{
    #[Unauthenticated]
    public function health(): JsonResponse
    {
        return response()->json(['status' => 'ok']);
    }
}
