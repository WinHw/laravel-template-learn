<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginAccountRequest;
use App\Http\Requests\SignupAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Resources\AccountResource;
use App\Models\Account;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Hash;
use Psy\Util\Json;

class AccountController1 extends Controller
{
    public function retrieve(): ResourceCollection
    {
        $accounts = Account::query();

        return AccountResource::collection($accounts->paginate(15));
    }

    public function retrieveSoftDeleted(): JsonResponse
    {
        $accounts = Account::onlyTrashed()->get();

        return response()->json([
            'data' => $accounts,
        ], empty($accounts) ? 204 : 200);
    }

    public function signup(SignupAccountRequest $request): AccountResource
    {
        $input = $request->validated();
        $input['password'] = Hash::make($input['password']);

        $account = new Account($input);
//        $account->fill($input);
        $account->save();

        return (new AccountResource($account))->additional([
            'message' => 'x'
        ]);
    }

    public function login(LoginAccountRequest $request): JsonResponse
    {
        $input = $request->validated();
        $account = Account::where('email', $input['credential'])
            ->orWhere('username', $input['credential'])
            ->first();

        if (!is_null($account) && Hash::check($input['password'], $account->password)) {
            return response()->json([
                'data' => $account->only(['id', 'username']),
            ]);
        } else {
            return response()->json([
                'data' => $account,
                'message' => 'Login credential or password is invalid.'
            ], 401);
        }
    }

    public function updateByID(UpdateAccountRequest $request, Account $account): JsonResponse
    {
        $message = null;
        if (is_null($account)) {
            return response()->json([
                'message' => 'Account ID is invalid.'
            ], 404);
        } else {
            $input = $request->validated();
            $account->fill($input);
            if ($account->isDirty(['username', 'email'])) $account->save();
            else $message = "Username or email is still using old values.";

            return response()->json([
                'message' => $message ?? 'Field(s) successfully updated.'
            ], $message ? 200 : 202);
        }
    }

    public function selfSoftDelete(Request $request): JsonResponse
    {
        $input = $request->validate([
            'id' => 'required|string',
            'password' => 'required|string',
        ]);
        $account = Account::find($input['id']);

        if (!is_null($account) && Hash::check($input['password'], $account->password)) {
            $id = $account->id;
            $account->delete();
            return response()->json([
                'message' => "Account regarding ID {$id} has been soft-deleted."
            ]);
        } else {
            return response()->json([
                'message' => 'Account ID is not found or password is invalid.'
            ], 401);
        }
    }

    public function softDelete(Request $request, $id): JsonResponse
    {
        $account = Account::find($id);

        if (!is_null($account)) {
            $id = $account->id;
            $account->delete();
            return response()->json([
                'message' => "Account regarding ID {$id} has been soft-deleted."
            ]);
        } else {
            return response()->json([
                'message' => 'Account ID is not found.'
            ], 404);
        }
    }

    public function selfForceDelete(Request $request): JsonResponse
    {
        $input = $request->validate([
            'id' => 'required|string',
            'password' => 'required|string',
        ]);
        $account = Account::find($input['id']);

        if (!is_null($account) && Hash::check($input['password'], $account->password)) {
            $id = $account->id;
            $account->forceDelete();
            return response()->json([
                'message' => "Account regarding ID {$id} has been force-deleted."
            ]);
        } else {
            return response()->json([
                'message' => 'Account ID is not found or password is invalid.'
            ], 401);
        }
    }
}
