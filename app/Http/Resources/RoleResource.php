<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

class RoleResource extends JsonResource
{
    public function toArray($request): array
    {
        $body = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        if (! $this->whenLoaded('permissionGroups') instanceof MissingValue) {
            $body['permission_groups'] = $this->permissionGroups->map(fn ($x) => $x['id']);
        }

        return $body;
    }
}
