<?php

namespace App\Console\Commands;

use App\Models\Employee;
use App\Models\Permission;
use App\Models\PermissionGroup;
use App\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AclSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'acl:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync ACL to database';

    private int $changes = 0;

    private string $guard = 'employee_api';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $this->syncPermissions();
        $this->syncPermissionGroups();
        $this->syncRoles();

        return Command::SUCCESS;
    }

    private function syncPermissions(): void
    {
        $this->changes = 0;
        $permission_ids = config('acl.permissions');
        foreach ($permission_ids as $permission_id) {
            $wheres = ['guard_name' => $this->guard, 'id' => $permission_id];
            $permission = Permission::where($wheres)->first();
            if ($permission === null) {
                $permission = new Permission();
                $permission->id = $wheres['id'];
                $permission->guard_name = $wheres['guard_name'];
                $permission->save();
                $this->changes++;
            }
        }
        $this->info("permission synced with $this->changes change(s)");

        $unused_permissions = Permission::whereGuardName($this->guard)->whereNotIn('id', $permission_ids)->get();
        if ($unused_permissions->count() > 0) {
            $this->info("\nbelow are unused permissions");
            dump($unused_permissions->pluck('id')->toArray());
            if ($this->confirm('Delete unused permissions?')) {
                $count = Permission::whereGuardName($this->guard)->whereNotIn('id', $permission_ids)->delete();
                $this->info("unused permission deleted with $count change(s)");
            }
        }
    }

    private function syncPermissionGroups(): void
    {
        $this->changes = 0;
        $permission_group_ids = [];
        $permission_group_objects = config('acl.permission_groups');
        foreach ($permission_group_objects as $category => $category_permission_group_ids) {
            foreach ($category_permission_group_ids as $permission_group_id => $permission_ids) {
                $wheres = ['guard_name' => $this->guard, 'id' => $permission_group_id];
                $permission_group = PermissionGroup::where($wheres)->first();
                if ($permission_group === null) {
                    $permission_group = new PermissionGroup();
                    $permission_group->id = $wheres['id'];
                    $permission_group->guard_name = $wheres['guard_name'];
                    $this->changes++;
                }
                $permission_group->category = $category;
                $permission_group->save();

                $permissions = Permission::whereGuardName($this->guard)->whereIn('id', $permission_ids)->get();
                $permission_group->permissions()->sync($permissions);

                $permission_group_ids[] = $permission_group_id;
            }
        }
        DB::table('role_has_permission_groups')
            ->whereNotIn('permission_group_id', $permission_group_ids)
            ->delete();
        $this->info("permission group synced with $this->changes change(s)");

        $unused_permission_groups = PermissionGroup::whereGuardName($this->guard)->whereNotIn('id', $permission_group_ids)->get();
        if ($unused_permission_groups->count() > 0) {
            $this->info("\nbelow are unused permission groups");
            dump($unused_permission_groups->pluck('id')->toArray());
            if ($this->confirm('Delete unused permission groups?')) {
                $count = PermissionGroup::whereGuardName($this->guard)->whereNotIn('id', $permission_group_ids)->delete();
                $this->info("unused permission group deleted with $count change(s)");
            }
        }
    }

    private function syncRoles(): void
    {
        $wheres = ['guard_name' => $this->guard, 'name' => 'SUPER ADMIN'];
        $role = Role::where($wheres)->first();
        if ($role === null) {
            $role = new Role();
            $role->name = $wheres['name'];
            $role->guard_name = $wheres['guard_name'];
            $role->save();
            $this->info('SUPER ADMIN role created');
        }
        $permission_group_ids = PermissionGroup::whereGuardName($this->guard)->pluck('id');
        $permission_ids = DB::table('permission_group_has_permissions')
            ->whereIn('permission_group_id', $permission_group_ids)
            ->distinct()->pluck('permission_id');
        $role->permissionGroups()->sync($permission_group_ids);
        $role->permissions()->sync($permission_ids);

        $other_roles = Role::where('guard_name', '=', $this->guard)
            ->where('name', '<>', 'SUPER ADMIN')->get();

        foreach ($other_roles as $other_role) {
            $permission_group_ids = $other_role->permissionGroups()->pluck('permission_group_id');
            $permission_ids = DB::table('permission_group_has_permissions')
                ->whereIn('permission_group_id', $permission_group_ids)
                ->distinct()->pluck('permission_id');
            $other_role->permissions()->sync($permission_ids);
        }

        $admin = Employee::whereEmail('admin@springkraf.com')->firstOrFail();
        $admin->assignRole($role);
        $this->info('SUPER ADMIN role assigned to admin@springkraf.com');
        $this->info("Other Role synced with {$other_roles->count()} change(s)");
    }
}
