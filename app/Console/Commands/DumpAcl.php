<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class DumpAcl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'acl:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump ACL to file (default to typescript)';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $fileName = realpath(__DIR__.'/../../../dumps').'/authorizationRules.ts';
        $file = fopen($fileName, 'w');
        fwrite($file, 'export enum AuthorizationRules {'.PHP_EOL);
        foreach (config('acl.permissions') as $permission) {
            $key = implode(' ', array_slice(preg_split('/[\.,-]+/', $permission), 1));
            $key = str_replace(' ', '', Str::title($key));
            fwrite($file, "  {$key} = '{$permission}',".PHP_EOL);
        }
        fwrite($file, '  NoAuthorization = "noauth",'.PHP_EOL);
        fwrite($file, '}'.PHP_EOL);
        fclose($file);

        return Command::SUCCESS;
    }
}
