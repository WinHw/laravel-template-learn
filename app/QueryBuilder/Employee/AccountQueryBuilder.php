<?php

namespace App\QueryBuilder\Employee;

use App\Models\Account;
use App\QueryBuilder\Base\BaseQueryBuilder;
use App\QueryBuilder\Sorts\InsensitiveSort;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;

class AccountQueryBuilder extends BaseQueryBuilder
{
    public function __construct()
    {
        parent::__construct(new Account());
    }

    public function getBuilder(...$args): Builder
    {
        return Account::query();
    }

    function getAllowedFiltersResponse(): array
    {
        return [
            'partialText.username',
            'partialText.email',
        ];
    }

    function getAllowedFilters(): array
    {
        return [
            AllowedFilter::partial('username'),
            AllowedFilter::partial('email'),
        ];
    }

    function getFilterOptions(): array
    {
        return [];
    }

    function getAllowedSortsResponse(): array
    {
        return [
            'username',
        ];
    }

    function getAllowedSorts(): array
    {
        return [
            AllowedSort::custom('username', new InsensitiveSort()),
        ];
    }

    function getDefaultSort(): string|AllowedSort
    {
        return AllowedSort::custom('username', new InsensitiveSort());
    }
}
