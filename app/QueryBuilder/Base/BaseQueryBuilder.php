<?php

namespace App\QueryBuilder\Base;

use App\Http\Helpers\QueryBuilderHelpers\FilterComposer;
use App\Http\Helpers\QueryBuilderHelpers\SortComposer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

abstract class BaseQueryBuilder
{
    public function __construct(protected Model $model)
    {
    }

    public function getQueryBuilder(...$args): QueryBuilder
    {
        $queryBuilder = QueryBuilder::for($this->getBuilder(...$args))
            ->allowedFilters($this->getAllowedFilters())
            ->allowedSorts($this->getAllowedSorts())
            ->defaultSort($this->getDefaultSort());
        $table = $this->model->getTable();
        return $queryBuilder->select("$table.*");
    }

    public function getResource(Request $request): array
    {
        return [
            'filters' => FilterComposer::getFilter(
                $this->getAllowedFiltersResponse(),
                $request,
                $this->getFilterOptions()
            ),
            'sorts' => SortComposer::getSort(
                $this->getAllowedSortsResponse(),
                $this->getDefaultSort(),
                $request->input('sort')
            ),
        ];
    }

    abstract public function getBuilder(...$args): Builder;

    abstract function getAllowedFiltersResponse(): array;
    abstract function getAllowedFilters(): array;
    abstract function getFilterOptions(): array;
    abstract function getAllowedSortsResponse(): array;
    abstract function getAllowedSorts(): array;
    abstract function getDefaultSort(): string|AllowedSort;
}
