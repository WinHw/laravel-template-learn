<?php

namespace App\Providers;

use Hidehalo\Nanoid\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
//        if ($this->app->environment('local')) {
//            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
//            $this->app->register(TelescopeServiceProvider::class);
//        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Str::macro('nanoid', function ($size = 10): string {
            return (new Client($size))->formattedId('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        });
    }
}
