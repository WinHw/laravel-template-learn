<?php

namespace Tests\Feature;

use Tests\TestCase;

class ExampleTest extends TestCase
{
    public function test_the_application_returns_a_successful_response(): void
    {
        $response = $this->getJson('/api/user/health');
        $response->assertOk();

        $response = $this->getJson('/api/employee/health');
        $response->assertOk();
    }

    public function test_user_login(): void
    {
        $response = $this->postJson('/api/user/auth/login', [
            'username' => 'erwin.zhang@springkraf.com',
            'password' => 'secret',
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                'token_type',
                'expires_in',
                'access_token',
                'refresh_token',
            ],
        ]);
    }

    public function test_employee_login(): void
    {
        $response = $this->postJson('/api/employee/auth/login', [
            'username' => 'admin@springkraf.com',
            'password' => 'secret',
        ]);
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                'token_type',
                'expires_in',
                'access_token',
                'refresh_token',
            ],
        ]);
    }
}
