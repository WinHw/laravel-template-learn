<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'contrib/discord.php';

set('app', 'LARAVEL WEB');
set('keep_releases', 5);
set('default_timeout', 600);
set('writable_mode', 'chmod');

set('discord_webhook', 'https://discord.com/api/webhooks/1090310134632300655/2fwew2fO3AZF3kzILXANJNcfH_wNE8rIpD3TOTbbRWfI9nERjw5_LU0UEhuwzoEjbooX');
before('deploy', 'discord:notify');
after('deploy:failed', 'discord:notify:failure');
after('deploy:success', 'discord:notify:success');

set('discord_notify_text', fn () => ['content' => parse('**{{app}}** - **{{user}}** is deploying branch `{{branch}}` to **{{target}}** :information_source:')]);
set('discord_success_text', fn () => ['content' => parse('**{{app}}** - **{{user}}** has deployed branch `{{branch}}` to **{{target}}** successfully :white_check_mark:')]);
set('discord_failure_text', fn () => ['content' => parse('**{{app}}** - **{{user}}** has failed to deploy branch `{{branch}}` to **{{target}}** :no_entry_sign:')]);

// Project repository
set('repository', 'git@gitlab.com:springkraf/core/laravel-template.git');
set('update_code_strategy', 'clone');

// Hosts

host('')
    ->setLabels(['stage' => 'production'])
    ->setHostname('')
    ->setRemoteUser('springkraf')
    ->setIdentityFile('~/.ssh/id_rsa')
    ->setDeployPath('~/{{alias}}')
    ->set('stage', 'production')
    ->set('branch', 'master');

host('')
    ->setLabels(['stage' => 'staging'])
    ->setHostname('')
    ->setRemoteUser('springkraf-dev')
    ->setIdentityFile('~/.ssh/id_rsa')
    ->setDeployPath('~/{{alias}}')
    ->set('composer_options', '--verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader')
    ->set('stage', 'staging')
    ->set('branch', 'staging');

// Tasks

task('yarn', function () {
    run('cd {{release_path}} && yarn');
});
after('deploy:vendors', 'yarn');

task('app:restart', function () {
//    run('sudo systemctl restart php8.2-fpm.service');
    run('sudo supervisorctl restart {{alias}}_server');
});
after('deploy:symlink', 'app:restart');

task('queue:restart', function () {
    run('sudo supervisorctl restart {{alias}}_horizon');
});
after('deploy:symlink', 'queue:restart');

//task('apidocs:generate', function () {
//    run('cd {{release_path}} && {{bin/php}} artisan scribe:generate');
//})->select('stage=staging');
//after('deploy:symlink', 'apidocs:generate');

task('artisan:migrate', artisan('migrate --force', ['skipIfNoEnv']))->once();

task('deploy:rollback', [
    'rollback',
    'app:restart',
]);

task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:config:cache',
    'artisan:route:cache',
    'artisan:view:cache',
    'artisan:event:cache',
    'artisan:migrate',
    'deploy:publish',
]);

after('deploy:failed', 'deploy:unlock');

//desc('Tag current commit with release version');
//task('git:tag-current-commit', function () {
//    run("cd {{release_path}} && git tag {{release_name}} && git push origin {{release_name}}");
//})->select('stage=production')->once();
//after('deploy:success', 'git:tag-current-commit');
