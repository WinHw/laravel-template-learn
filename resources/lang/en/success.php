<?php

return [
    'login_success' => 'Login Success',
    'refresh_token_success' => 'Refresh Token Success',
    'logout_success' => 'Logout Success',
    'change_password_success' => 'Change Password Success',

    // Account
    'store_account_success' => 'Store Account Success',
    'login_account_success' => 'Login Account Success',
    'update_account_success' => 'Update Account Success',
    'destroy_account_success' => 'Destroy Account Success',
];
