<?php

return [
    'update_delete_failed' => 'Failed to update/delete, data might have relation with other data.',
    'incorrect_credentials' => 'The provided credentials is incorrect.',
    'incorrect_password' => 'The provided password is incorrect.',
    'incorrect_refresh_token' => 'The provided refresh token is incorrect.',
    'logout_failed' => 'Logout failed.',
    'copy_data_to_temp_failed_url_not_valid' => 'Copy data to temp failed, url is not valid',
    'change_password_success' => 'Change password success.',

    // Account
    'login_account_failed' => 'Login account failed, credential or password is invalid.',
    'update_account_error' => 'Update account error.',
];
